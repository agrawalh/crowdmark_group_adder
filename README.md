All CSE260 students, it was a pain to manually add group members every time. So you can use this script to add group members automatically.

# Setup: 

Go to terminal and install pynput:

pip install pynput

# how to use

1) Enter your group members' email addresses 
2) Change the x & y coordinates as mentioned in the comments in code

Enjoy CSE260!