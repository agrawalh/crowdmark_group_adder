from pynput.keyboard import Key, Controller as KeyboardController
from pynput.mouse import Button, Controller as MouseController
from time import sleep

keyboard = KeyboardController()
mouse = MouseController()

def msclick(x, y):
    mouse.position = (x, y)
    mouse.click(Button.left, 1)

# Click on add group members
msclick(1069, 718)      # Coordinates of "Add group Members"

sleep(1)

emails = ['karanjik@msu.edu', 'harrel49@msu.edu', 'nanamori@msu.edu', 
'quinnluc@msu.edu', 'huhaoyan@msu.edu', 'joshipr4@msu.edu', 'chenduor@msu.edu', 'cardime2@msu.edu',
'tochikia@msu.edu', 'nitkiew2@msu.edu']

y = 538         # initial y coordinate of the search bar in group members

# Click on add
for i, email in enumerate(emails):
    msclick(1110, y)                # x coordinate of the search bar in group members
    sleep(1)
    
    keyboard.type(email)
    sleep(2)
    keyboard.press(Key.enter)
    keyboard.release(Key.enter)
    if (i+1) % 2 == 0:
        y -= 25

# Use the line below to get the coordinates for on time setup
# print ("Current position: " + str(mouse.position))